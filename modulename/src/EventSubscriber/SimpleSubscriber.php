<?php

/**
 * @file
 * Contains Drupal\modulename\EventSubscriber\SimpleSubscriber.
 */

namespace Drupal\modulename\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\page_example\Event\SimplePageEvent;

class SimpleSubscriber implements EventSubscriberInterface {

  /** @var \Drupal\Core\Messenger\MessengerInterface $messenger */
  private $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SimplePageEvent::SIMPLE_PAGE_LOAD][] = ['onSimplePageLoad', 0];
    return $events;
  }

  /**
   * This method is called whenever the SimplePageEvent::SIMPLE_PAGE_LOAD even
   * is dispatched.
   *
   * @param \Drupal\page_example\Event\SimplePageEvent $event
   *   Event.
   */
  public function onSimplePageLoad(SimplePageEvent $event) {
    $evMessage = $event->getMessage();
    $evMessage .= ' Modulename Dispatched OK! (with injection)';
    $this->messenger->addMessage($evMessage);
  }

}
