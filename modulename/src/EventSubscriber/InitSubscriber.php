<?php

/**
 * @file
 * Contains Drupal\modulename\EventSubscriber\InitSubscriber.
 */

namespace Drupal\modulename\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountProxy;

class InitSubscriber implements EventSubscriberInterface {

  /** @var \Drupal\Core\Session\AccountProxy $currentUser */
  private $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(AccountProxy $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['addAccessAllowOriginHeaders'];
    return $events;
  }

  /**
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   */
  public function addAccessAllowOriginHeaders(FilterResponseEvent $event) {
    if ($this->currentUser->isAnonymous()) {
      $response = $event->getResponse();
      $response->headers->set('Access-Control-Allow-Origin', '*');
    }
  }

}
