<?php

namespace Drupal\modulename\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\modulename\Controller\Forecast;

/**
 * Provides a 'ForecastBlock' block.
 *
 * @Block(
 *  id = "forecast_block",
 *  admin_label = @Translation("Forecast block"),
 * )
 */
class ForecastBlock extends BlockBase implements ContainerFactoryPluginInterface {

  use RedirectDestinationTrait;

  /**
   * The FormBuilder object.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new SwitchUserBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['forecast_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your API Key'),
      '#description' => $this->t('API key'),
      '#default_value' => isset($this->configuration['forecast_api_key']) ?
        $this->configuration['forecast_api_key'] : '',
    ];
    $form['location_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location lat'),
      '#description' => $this->t('location lat'),
      '#default_value' => isset($this->configuration['location_lat']) ?
        $this->configuration['location_lat'] : '',
    ];
    $form['location_long'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location long'),
      '#description' => $this->t('location long'),
      '#default_value' => isset($this->configuration['location_long']) ?
        $this->configuration['location_long'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['forecast_api_key'] = $form_state->getValue('forecast_api_key');
    $this->configuration['location_lat'] = $form_state->getValue('location_lat');
    $this->configuration['location_long'] = $form_state->getValue('location_long');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['forecast_block_inputtext']['#markup'] = '<p>' . $this->configuration['title'] . '</p>';
    $forecast = $this->getForecast();
    $timezone = !empty($forecast['timezone']) ? $forecast['timezone'] : '';

    if (!empty($timezone)) {
      $build['forecast_block_inputtext']['#markup'] .= '<p>' . $timezone . '</p>';
    }

    $build['forecast_block_inputtext']['#markup'] .= '<p>';
    $build['forecast_block_inputtext']['#markup'] .= $this->t(
      'Forecast is @summary with temperature of @temperature dec C.',
      [
        '@summary' => $forecast['summary'],
        '@temperature' => $forecast['temperature'],
      ]
    );
    $build['forecast_block_inputtext']['#markup'] .= '</p>';

    return $build;
  }

  /**
   * Returns forecast data.
   *
   * @return array
   *   Forecast data.
   */
  public function getForecast() {
    $data = [
      'summary' => 'xxx',
      'temperature' => 'xxx',
      'timezone' => '',
    ];

    $key = $this->configuration['forecast_api_key'];

    if (empty($key)) {
      return $data;
    }

    $forecast = new Forecast($key);
    $long = $this->configuration['location_long'];
    $lat = $this->configuration['location_lat'];
    $response = !empty($long) && !empty($lat) ? $forecast->get($lat, $long, NULL, ['units' => 'auto']) : '';

    if (empty($response) || !isset($response->currently->summary) && !isset($response->currently->temperature)) {
      return $data;
    }

    $data['summary'] = $response->currently->summary;
    $data['temperature'] = $response->currently->temperature;
    $data['timezone'] = $response->timezone;
    return $data;
  }

}
