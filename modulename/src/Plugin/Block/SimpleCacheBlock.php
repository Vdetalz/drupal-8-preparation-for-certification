<?php

namespace Drupal\modulename\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webprofiler\Cache\CacheBackendWrapper;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\webprofiler\Entity\EntityManagerWrapper;

/**
 * Provides a 'SimpleCacheBlock' block.
 *
 * @Block(
 *  id = "simple_cache_block",
 *  admin_label = @Translation("Simple cache block"),
 * )
 */
class SimpleCacheBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\webprofiler\Cache\CacheBackendWrapper definition.
   *
   * @var \Drupal\webprofiler\Cache\CacheBackendWrapper
   */
  protected $cacheDefault;

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Drupal\webprofiler\Entity\EntityManagerWrapper definition.
   *
   * @var \Drupal\webprofiler\Entity\EntityManagerWrapper
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SimpleCacheBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\webprofiler\Cache\CacheBackendWrapper $cache_default
   *   The CacheBackendWrapper service.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
   *   The QueryFactory service.
   * @param \Drupal\webprofiler\Entity\EntityManagerWrapper $entityTypeManager
   *   The EntityManagerWrapper service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CacheBackendWrapper $cache_default,
    QueryFactory $entityQuery,
    EntityManagerWrapper $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cacheDefault = $cache_default;
    $this->entityQuery = $entityQuery;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cache.default'),
      $container->get('entity.query'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['number_nodes'] = [
      '#type' => 'number',
      '#title' => $this->t('InputText'),
      '#description' => $this->t('Just an input text'),
      '#default_value' => isset($this->configuration['number_nodes']) ? $this->configuration['number_nodes'] : 5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number_nodes'] = $form_state->getValue('number_nodes');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];
    $nodes = $this->getLastNodes();
    $build['simple_cache_block_simple_text']['#markup'] = '<p>' . $this->t(
        'Total: @total',
        [
          '@total' => $this->configuration['number_nodes'],
        ]
      )
      . '</p>';
    foreach ($nodes as $node) {
      $build['simple_cache_block_simple_text']['#markup'] .= '<p>' . $node['data'] . '</p>';
    }
    return $build;
  }

  /**
   * Returns count 'number_nodes' $nodes.
   *
   * @return array
   *   Nodes.
   */
  public function getLastNodes() {
    if ($cache = $this->cacheDefault->get('cache_simple_nodes')) {
      return $cache->data;
    }
    else {
      $titles = [];
      $tags = ['node_list'];
      $nids = $this->executeQueryNode($this->configuration['number_nodes']);
      $nodes = $this->entityTypeManager->getStorage('node')
        ->loadMultiple($nids);

      foreach ($nodes as $node) {
        $nid = $node->get('nid')->value;
        $titles[$nid]['data'] = $this->t(
          'Node - @id Title - @title',
          [
            '@id' => $nid,
            '@title' => $node->get('title')->value,
          ]
        );
        $titles[$nid]['created'] = $node->get('created')->value;
        $tags[] = 'node:' . $nid;
      }

      $this->cacheDefault->set('cache_simple_nodes', $titles, CacheBackendInterface::CACHE_PERMANENT, $tags);
      return $titles;
    }
  }

  /**
   * Returns executed query.
   *
   * @param int $pager
   *   Count entities.
   *
   * @return array|int
   *   Execute query.
   */
  private function executeQueryNode($pager = 1) {
    $query = $this->entityQuery->get('node');
    $query->condition('status', 1)
      ->sort('created', 'DESC')
      ->pager($pager);
    return $query->execute();
  }

}
