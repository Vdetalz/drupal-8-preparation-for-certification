<?php

namespace Drupal\modulename\Controller;

/**
 * API's Class Forecast.
 *
 * @package Drupal\modulename\Controller
 */
class Forecast {
  /**
   * API's Endpoint.
   */
  const API_ENDPOINT = 'https://api.darksky.net/forecast/';

  /**
   * API key.
   *
   * @var string
   */
  private $apiKey;

  /**
   * Forecast constructor.
   *
   * @param string $apiKey
   *   API key.
   */
  public function __construct($apiKey) {
    $this->apiKey = $apiKey;
  }

  /**
   * Do request.
   *
   * @param string $latitude
   * @param string $longitude
   * @param string $time
   *   Time.
   * @param array $options
   *   API's options.
   *
   * @return mixed
   *   Response.
   */
  private function request($latitude, $longitude, $time = NULL, array $options = []) {
    $request_url = self::API_ENDPOINT
      . $this->apiKey
      . '/'
      . $latitude
      . ','
      . $longitude
      . ((is_null($time)) ? '' : ',' . $time);

    if (!empty($options)) {
      $request_url .= '?' . http_build_query($options);
    }

    $response = json_decode(file_get_contents($request_url));
    $response->headers = $http_response_header;
    return $response;
  }

  /**
   * Do request.
   *
   * @param string $latitude
   * @param string $longitude
   * @param string $time
   *   Time.
   * @param array $options
   *   API's options.
   *
   * @return mixed
   *   Response.
   */
  public function get($latitude, $longitude, $time = NULL, array $options = []) {
    return $this->request($latitude, $longitude, $time, $options);
  }

}
