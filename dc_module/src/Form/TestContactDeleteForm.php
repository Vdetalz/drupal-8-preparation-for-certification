<?php

namespace Drupal\dc_module\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Test contact entities.
 *
 * @ingroup dc_module
 */
class TestContactDeleteForm extends ContentEntityDeleteForm {


}
