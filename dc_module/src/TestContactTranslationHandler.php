<?php

namespace Drupal\dc_module;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for test_contact.
 */
class TestContactTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
