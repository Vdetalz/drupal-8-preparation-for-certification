<?php

namespace Drupal\dc_module;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Test contact entity.
 *
 * @see \Drupal\dc_module\Entity\TestContact.
 */
class TestContactAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dc_module\Entity\TestContactInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished test contact entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published test contact entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit test contact entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete test contact entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add test contact entities');
  }

}
