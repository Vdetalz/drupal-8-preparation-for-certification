<?php

namespace Drupal\dc_module\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Test contact entities.
 *
 * @ingroup dc_module
 */
interface TestContactInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Test contact name.
   *
   * @return string
   *   Name of the Test contact.
   */
  public function getName();

  /**
   * Sets the Test contact name.
   *
   * @param string $name
   *   The Test contact name.
   *
   * @return \Drupal\dc_module\Entity\TestContactInterface
   *   The called Test contact entity.
   */
  public function setName($name);

  /**
   * Gets the Test contact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Test contact.
   */
  public function getCreatedTime();

  /**
   * Sets the Test contact creation timestamp.
   *
   * @param int $timestamp
   *   The Test contact creation timestamp.
   *
   * @return \Drupal\dc_module\Entity\TestContactInterface
   *   The called Test contact entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Test contact published status indicator.
   *
   * Unpublished Test contact are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Test contact is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Test contact.
   *
   * @param bool $published
   *   TRUE to set this Test contact to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\dc_module\Entity\TestContactInterface
   *   The called Test contact entity.
   */
  public function setPublished($published);

}
