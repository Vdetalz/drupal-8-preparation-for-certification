<?php

/**
 * @file
 * Contains test_contact.page.inc.
 *
 * Page callback for Test contact entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Test contact templates.
 *
 * Default template: test_contact.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_test_contact(array &$variables) {
  $test_contact = $variables['elements']['#test_contact'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
