<?php
/**
 * @file
 * Contains \Drupal\image_title_caption\Plugin\Field\FieldFormatter\ImageTitleCaption
 */

namespace Drupal\image_title_caption\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'image_title_caption' formatter.
 *
 * @FieldFormatter(
 *   id = "image_title_caption",
 *   label = @Translation("Image with caption from title"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageTitleCaption extends ImageFormatter {
  /**
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param string $langcode
   *
   * @return array
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $settings = $this->getSettings();

    foreach ($elements as &$element) {
      if ($settings['strong'] == 1) {
        $element['#theme'] = 'image_title_caption_formatter';
      }
      elseif ($settings['strong'] == 0) {
        $element['#theme'] = 'image_title_caption_strong_formatter';
      }
    }

    return $elements;
  }

  /**
   * @return array
   */
  public static function defaultSettings() {
    return ['strong' => 0,] + parent::defaultSettings();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|mixed
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $active   = array(0 => t('Strong'), 1 => t('No strong'),);

    $elements['strong'] = array(
      '#type'          => 'radios',
      '#title'         => t('Strong status'),
      '#default_value' => $this->getSetting('strong'),
      '#options'       => $active,
      '#description'   => t('When a strong is active, caption will be bold.'),
    );

    return $elements;
  }

  /**
   * @return array|\string[]
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $strong    = $this->getSetting('strong') == 0 ? t('Strong') : t('No Strong');
    $summary[] = t('Strong status: @strong ', array('@strong' => $strong));

    return $summary;
  }
}